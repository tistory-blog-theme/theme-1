window.onload = function(){
  // set click event - category event
  document.querySelectorAll('.category .menu').forEach(menu => {
    menu.addEventListener('click', function(e){
      e.currentTarget.classList.toggle('on');
    })
  });

  // set click event - category link
  document.querySelectorAll('.category .link').forEach(menu => {
    menu.addEventListener('click', function(e){
      var isOn = document.querySelector('.category .link.on');
      if(isOn){
        document.querySelector('.category .link.on').classList.remove('on');
      }
      e.currentTarget.classList.toggle('on');
    })
  });

  expandAll();
}

// Expand All
function expandAll () {
  document.querySelectorAll('.category .menu').forEach(menu => {
    menu.classList.add('on');
  })
}